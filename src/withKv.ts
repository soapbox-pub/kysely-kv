import { DenoSqlite3Dialect } from '@soapbox/kysely-deno-sqlite';
import { Kysely } from 'kysely';
import { KyselyKv } from './KyselyKv.ts';
import { Database as Sqlite } from '@db/sqlite';
import { createPgKysely, PostgresConnectionOptions } from './postgres-kysely-driver.ts';

export const withSqliteKyselyKv = async (f: (kv: Deno.Kv) => Promise<void>) => {
  const kysely = new Kysely({
    dialect: new DenoSqlite3Dialect({
      database: new Sqlite(':memory:'),
    }),
  });
  const kv = new KyselyKv(kysely);
  await kv.migrate();
  await f(kv);
  kv.close();
};

export const withPostgresKyselyKv = (opts: PostgresConnectionOptions) => {
  return async (f: (kv: Deno.Kv) => Promise<void>) => {
    const kysely = createPgKysely(opts);
    const kv = new KyselyKv(kysely);
    await kv.migrate();
    await f(kv);
    kv.close();
  };
};

export const withDenoKv = async (f: (kv: Deno.Kv) => Promise<void>) => {
  const kv = await Deno.openKv(':memory:');
  await f(kv);
  kv.close();
};
