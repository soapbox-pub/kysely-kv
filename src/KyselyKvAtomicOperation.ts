import { Transaction } from 'kysely';
import { KyselyKv, KyselyKvTables } from './KyselyKv.ts';

interface AtomicOperationEvent {
  kind: 'check' | 'sum' | 'min' | 'max' | 'set' | 'delete';
  key: Deno.KvKey;
  check?: string | null;
  // deno-lint-ignore no-explicit-any
  value?: any;
}

export class KyselyKvAtomicOperation implements Deno.AtomicOperation {
  private parent: KyselyKv;
  private events: AtomicOperationEvent[] = [];
  private closed: boolean = false;

  /**
   * DO NOT CONSTRUCT THIS CLASS: USE `KyselyKv::atomic()` INSTEAD!
   *
   * Creates a new AtomicOperation. This class basically queues operations done to it
   * until commit() is called, at which point it makes a Kysely transaction out of them
   * and runs them all at once.
   * @param parent The KyselyKv instance that this AtomicOperation was initiated by.
   */
  constructor(parent: KyselyKv) {
    this.parent = parent;
  }

  /**
   * Checks if the transaction has already been committed/cancelled.
   * MUST BE CALLED IN EVERY TRANSACTION METHOD!
   */
  private checkClosed() {
    if (this.closed) throw new Error('Tried to perform operation with committed txn!');
  }

  check(...checks: Deno.AtomicCheck[]): this {
    this.checkClosed();
    for (const check of checks) {
      this.events.push({
        kind: 'check',
        check: check.versionstamp,
        key: check.key,
      });
    }
    return this;
  }
  mutate(...mutations: Deno.KvMutation[]): this {
    this.checkClosed();
    for (const mutation of mutations) {
      if (mutation.type === 'delete') {
        this.events.push({ kind: 'delete', key: mutation.key });
      } else {
        this.events.push({ kind: mutation.type, key: mutation.key, value: mutation.value });
      }
    }
    return this;
  }
  sum(key: Deno.KvKey, n: bigint): this {
    this.checkClosed();
    this.mutate({ type: 'sum', key, value: new Deno.KvU64(n) });
    return this;
  }
  min(key: Deno.KvKey, n: bigint): this {
    this.checkClosed();
    this.mutate({ type: 'min', key, value: new Deno.KvU64(n) });
    return this;
  }
  max(key: Deno.KvKey, n: bigint): this {
    this.checkClosed();
    this.mutate({ type: 'max', key, value: new Deno.KvU64(n) });
    return this;
  }
  set(key: Deno.KvKey, value: unknown, _options?: { expireIn?: number | undefined } | undefined): this {
    // TODO: respect expireIn!
    this.checkClosed();
    this.mutate({ type: 'set', key, value });
    return this;
  }
  delete(key: Deno.KvKey): this {
    this.checkClosed();
    this.mutate({ type: 'delete', key });
    return this;
  }
  enqueue(
    _value: unknown,
    _options?: {
      delay?: number | undefined;
      keysIfUndelivered?: Deno.KvKey[] | undefined;
      backoffSchedule?: number[] | undefined;
    } | undefined,
  ): this {
    // TODO: implement enqueue
    throw new Error('Method not implemented.');
  }

  commit(): Promise<Deno.KvCommitResult | Deno.KvCommitError> {
    this.checkClosed();
    this.closed = true;
    return new Promise((resolve) => {
      this.parent.runTransaction(async (f) => {
        let lastVersion = BigInt(-1);

        this.events.sort((a, b) => (a.kind === 'check' && b.kind !== 'check') ? 1 : 0);
        for (const event of this.events) {
          const k = KyselyKv.encodeKey(event.key);
          switch (event.kind) {
            case 'check': {
              const res = await f
                .selectFrom('kv')
                .select('version')
                .where('k', '=', k)
                .executeTakeFirst();

              const nullCheckFailed = event.check === null && res;
              const versionCheckFailed = !res || BigInt('0x' + event.check!) !== res.version;

              if (nullCheckFailed || versionCheckFailed) {
                resolve({ ok: false });
                throw new Error('Null check failed.');
              }
              break;
            }
            case 'sum': {
              lastVersion = await this.doMutation(f, k, (old) => old + event.value);
              break;
            }
            case 'min':
              lastVersion = await this.doMutation(
                f,
                k,
                (old, didExist) => didExist ? (old < event.value ? old : event.value) : event.value,
              );
              break;
            case 'max':
              lastVersion = await this.doMutation(
                f,
                k,
                (old, didExist) => didExist ? (old > event.value ? old : event.value) : event.value,
              );
              break;
            case 'set': {
              const version = await this.parent.getNextVersionstamp();
              const res = await f
                .insertInto('kv')
                .onDuplicateKeyUpdate({
                  k,
                  version,
                  v: KyselyKv.encodeVal(event.value!),
                })
                .returning('version')
                .executeTakeFirstOrThrow();
              lastVersion = res.version;
              break;
            }
            case 'delete':
              f.deleteFrom('kv').where('k', '=', k);
              break;
          }
        }

        if (lastVersion === BigInt(-1)) {
          lastVersion = await this.parent.getNextVersionstamp();
        }

        resolve({ ok: true, versionstamp: KyselyKv.encodeVersionstamp(lastVersion) });
      });
    });
  }

  /**
   * Performs a single mutation on the database.
   * @param f The transaction object being used in the commit.
   * @param k The key to mutate.
   * @param lambda The mutation to perform.
   * @returns The versionstamp of the newly mutated key.
   */
  private async doMutation(
    f: Transaction<KyselyKvTables>,
    k: string,
    lambda: (old: bigint, didExist: boolean) => bigint,
  ) {
    const r = await f
      .selectFrom('kv')
      .select('v')
      .where('k', '=', k)
      .executeTakeFirst();
    const version = await this.parent.getNextVersionstamp();

    const old: bigint = r ? KyselyKv.decodeVal(r.v) : BigInt(0);
    const res = await f
      .insertInto('kv')
      .onDuplicateKeyUpdate({ k, version, v: KyselyKv.encodeVal(lambda(old, !!r)) })
      .returning('version')
      .executeTakeFirstOrThrow();

    return res.version;
  }
}
