export class KyselyKvListIterator<T> implements Deno.KvListIterator<T> {
  private iter: AsyncGenerator<Deno.KvEntry<T>>;
  constructor(iter: AsyncGenerator<Deno.KvEntry<T>>) {
    this.iter = iter;
  }

  get cursor(): string {
    throw new Error('Method not implemented.');
  }

  async next(): Promise<IteratorResult<Deno.KvEntry<T>, undefined>> {
    const n = await this.iter.next();
    return n;
  }

  [Symbol.asyncIterator](): AsyncIterableIterator<Deno.KvEntry<T>> {
    return this;
  }
}
