import { assertEquals } from '@std/assert';
import fixtures from '../fixtures.json' with { type: 'json' };
import { withDenoKv, withPostgresKyselyKv, withSqliteKyselyKv } from './withKv.ts';

const compareResults = async (op: (kv: Deno.Kv) => unknown, withKv: typeof withDenoKv) =>
  await withDenoKv(async (dkv) => await withKv(async (kv) => assertEquals(await op(kv), await op(dkv))));

const _withPostgresKv = () => {
  return withPostgresKyselyKv({
    database: 'kysely-kv-testing',
    host: '127.0.0.1',
    port: 5432,
    password: '1234',
    username: 'postgres',
  });
};

Deno.test('KyselyKv.set and KyselyKv.get', async (t) => {
  const op = async (kv: Deno.Kv) => {
    await kv.set(['hello'], 'world');
    const { value } = await kv.get(['hello']);
    return value;
  };

  await t.step('with sqlite backend', async () => await compareResults(op, withSqliteKyselyKv));
  // await t.step('with postgres backend', async () => await compareResults(op, withPostgresKv()));
});

Deno.test('KyselyKv.getMany', async (t) => {
  const op = async (kv: Deno.Kv) => {
    await kv.set(['foo'], 'bar');
    await kv.set(['baz'], 'quux');
    return (await kv.getMany([['foo'], ['baz']]))
      .filter((v) => Boolean(v.value))
      .map((v) => v.value);
  };
  await t.step('with sqlite backend', async () => await compareResults(op, withSqliteKyselyKv));
  // await t.step('with postgres backend', async () => await compareResults(op, withPostgresKv()));
});

Deno.test('KyselyKv.list', async (t) => {
  for (const [name, test] of Object.entries(fixtures.listTests)) {
    const reverse = 'reverse' in test ? test['reverse'] : false;
    await t.step(
      name,
      async (t) => {
        const op = async (kv: Deno.Kv) =>
          await Array.fromAsync(kv.list(test.selector, { reverse }))
            .then((v) => v.map((e) => e.key));

        await t.step('with sqlite backend', async () => await compareResults(op, withSqliteKyselyKv));
        // await t.step('with postgres backend', async () => await compareResults(op, withPostgresKv()));
      },
    );
  }
});
