// deno-lint-ignore-file no-explicit-any
export const isPrefix = (obj: any): obj is { prefix: Deno.KvKey } => {
  return 'prefix' in obj && !('start' in obj || 'end' in obj);
};

export const isPrefixFrom = (obj: any): obj is { prefix: Deno.KvKey; start: Deno.KvKey } => {
  return 'prefix' in obj && 'start' in obj && !('end' in obj);
};

export const isPrefixUpto = (obj: any): obj is { prefix: Deno.KvKey; end: Deno.KvKey } => {
  return 'prefix' in obj && 'end' in obj && !('start' in obj);
};

export const isRange = (obj: any): obj is { start: Deno.KvKey; end: Deno.KvKey } => {
  return 'start' in obj && 'end' in obj && !('prefix' in obj);
};
