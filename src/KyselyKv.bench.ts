import { withDenoKv, withSqliteKyselyKv } from './withKv.ts';
import fixtures from '../fixtures.json' with { type: 'json' };

const bench = {
  set: (b: Deno.BenchContext) => async (kv: Deno.Kv) => {
    const key = [crypto.randomUUID()];
    b.start();
    await kv.set(key, 'hello world');
    b.end();
  },
  get: (b: Deno.BenchContext) => async (kv: Deno.Kv) => {
    const key = [crypto.randomUUID()];
    await kv.set(key, 'hello world');
    b.start();
    await kv.get(key);
    b.end();
  },
  getMany: (b: Deno.BenchContext) => async (kv: Deno.Kv) => {
    await kv.set(['foo'], 'bar');
    await kv.set(['baz'], 'quux');
    b.start();
    await kv.getMany([['foo'], ['baz']]);
    b.end();
  },
  list: (b: Deno.BenchContext) => async (kv: Deno.Kv) => {
    await Promise.all(fixtures.keysToInsert.map(async (key) => await kv.set(key, true)));
    b.start();
    await Array.fromAsync(kv.list({ 'prefix': ['abc'], 'end': ['abc', 'jkl'] }));
    b.end();
  },
  listReverse: (b: Deno.BenchContext) => async (kv: Deno.Kv) => {
    await Promise.all(fixtures.keysToInsert.map(async (key) => await kv.set(key, true)));
    b.start();
    await Array.fromAsync(kv.list({ 'prefix': ['abc'], 'end': ['abc', 'jkl'] }, { reverse: true }));
    b.end();
  },
};

const benches: (keyof typeof bench)[] = ['get', 'set', 'getMany', 'list', 'listReverse'];
for (const name of benches) {
  for (const backend of ['KyselyKv', 'Deno.Kv']) {
    const withKv = backend === 'KyselyKv' ? withSqliteKyselyKv : withDenoKv;
    Deno.bench(`${backend}.${name}`, async (b) => await withKv(bench[name](b)));
  }
}
