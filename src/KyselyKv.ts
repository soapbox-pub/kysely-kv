import { Kysely, NoResultError, SelectQueryBuilder, Transaction } from 'kysely';
import { pack, unpack } from 'msgpackr';
import { fromBufferKey, Key, toBufferKey } from 'ordered-binary';
import { KyselyKvAtomicOperation } from './KyselyKvAtomicOperation.ts';
import { bytesToHex, hexToBytes } from 'npm:@noble/hashes/utils';
import { isPrefix, isPrefixFrom, isPrefixUpto } from './utils.ts';
import { KyselyKvListIterator } from './KyselyKvListIterator.ts';

export interface KyselyKvTables {
  /**
   * A table only meant to have a single row, holding information
   * of the last versionstamp issued by the db.
   */
  last_version: {
    i: number;
    v: bigint;
  };

  /**
   * Where the magic happens: stores the actual keys/values/versions.
   * `version` is a bigint and not an int because it's a 10-byte number.
   */
  kv: {
    k: string;
    v: Uint8Array;
    version: bigint;
  };
}

/**
 * The main KyselyKv implementation.
 */
export class KyselyKv implements Deno.Kv {
  private kysely: Kysely<KyselyKvTables>;

  /**
   * Encode a value for storage in the database.
   * @param val The value to encode.
   * @returns A Uint8Array representing the encoded value.
   */
  // deno-lint-ignore no-explicit-any
  static encodeVal(val: any): Uint8Array {
    const buf = pack(val);
    return new Uint8Array(buf);
  }

  /**
   * Decode a value returned from the database.
   * @param buf The value to decode.
   * @returns The decoded value.
   */
  // deno-lint-ignore no-explicit-any
  static decodeVal(buf: Uint8Array): any {
    return unpack(buf);
  }

  /**
   * Encode a key for use in the database.
   * @param key The Deno.KvKey to encode.
   * @returns A string representing the encoded key.
   */
  static encodeKey(key: Deno.KvKey): string {
    return bytesToHex(toBufferKey(key as Key));
  }

  /**
   * Decode a key retrieved from the database into a Deno.KvKey.
   * @param k The key to decode.
   * @returns The decoded Deno.KvKey.
   */
  static decodeKey(k: string): Deno.KvKey {
    // @ts-ignore: Buffer is just Node.js' crappy nonstandard Buffer impl
    const key = fromBufferKey(hexToBytes(k));
    if (!key) throw new Error('Key was null. This should never happen.');
    return key as Deno.KvKey;
  }

  /**
   * Encode a versionstamp into a 10-byte (20 character) hex string.
   * @param n the bigint to encode
   * @returns the encoded versionstamp
   */
  static encodeVersionstamp(n: bigint): string {
    return n.toString(16).padStart(20, '0');
  }

  /**
   * Create a new KyselyKv instance. The Kysely instance is closed when
   * you call the close() method, as well as if you use the `using` keyword
   * in Typescript. You will want to call migrate() to set up the schemas
   * used by the key-value store.
   *
   * KyselyKv does not support the `eventual` consistency options in Deno KV -
   * all operations are performed with strong consistency.
   * @example
   * const kv = new KyselyKv({ ... });
   * await kv.migrate();
   * @param kysely The Kysely instance to use.
   */
  // deno-lint-ignore no-explicit-any
  constructor(kysely: Kysely<any>) {
    this.kysely = kysely;
  }

  async get<T = unknown>(
    key: Deno.KvKey,
  ): Promise<Deno.KvEntryMaybe<T>> {
    try {
      const { v, version } = await this.kysely
        .selectFrom('kv')
        .select(['v', 'version'])
        .where('k', '=', KyselyKv.encodeKey(key))
        .executeTakeFirstOrThrow();

      const value = KyselyKv.decodeVal(v);
      const versionstamp = KyselyKv.encodeVersionstamp(version);
      return { value: value as T, versionstamp, key };
    } catch (e) {
      if (!(e instanceof NoResultError)) throw e;
      return { value: null, key, versionstamp: null };
    }
  }

  async getMany<T extends readonly unknown[]>(
    keys: readonly [...{ [K in keyof T]: Deno.KvKey }],
  ): Promise<{ [K in keyof T]: Deno.KvEntryMaybe<T[K]> }> {
    const encodedKeys = keys.map(KyselyKv.encodeKey);
    const found = await this.kysely
      .selectFrom('kv')
      .select(['v', 'version', 'k'])
      .where((eb) => eb.or(encodedKeys.map((k) => eb('k', '=', k))))
      .orderBy('k asc')
      .execute();

    const m = new Map<string, (typeof found)[0]>();
    for (const itm of found) {
      m.set(itm.k, itm);
    }

    const res: Deno.KvEntryMaybe<unknown>[] = [];
    for (const key of encodedKeys) {
      const itm = m.get(key);
      if (itm) {
        res.push({
          key: KyselyKv.decodeKey(itm.k),
          value: KyselyKv.decodeVal(itm.v),
          versionstamp: KyselyKv.encodeVersionstamp(itm.version),
        });
      } else {
        res.push({
          key: KyselyKv.decodeKey(key),
          value: null,
          versionstamp: null,
        });
      }
    }

    return res as { [K in keyof T]: Deno.KvEntryMaybe<T[K]> };
  }

  async set(key: Deno.KvKey, value: unknown, _options?: { expireIn?: number }): Promise<Deno.KvCommitResult> {
    // TODO: Respect expiring keys!
    const version = await this.getNextVersionstamp();
    await this.kysely
      .insertInto('kv')
      .values({ version, k: KyselyKv.encodeKey(key), v: KyselyKv.encodeVal(value) })
      .execute();

    return { ok: true, versionstamp: KyselyKv.encodeVersionstamp(version) };
  }

  async delete(key: Deno.KvKey): Promise<void> {
    await this.kysely
      .deleteFrom('kv')
      .where('k', '=', KyselyKv.encodeKey(key))
      .execute();
  }

  private getQuery(
    selector: Deno.KvListSelector,
    reverse?: boolean,
  ): SelectQueryBuilder<KyselyKvTables, 'kv', KyselyKvTables['kv']> {
    let q = this.kysely.selectFrom('kv').selectAll();
    if (isPrefix(selector)) {
      q = q.where('k', 'like', KyselyKv.encodeKey(selector.prefix) + '%');
    } else if (isPrefixFrom(selector)) {
      q = q.where('k', 'like', KyselyKv.encodeKey(selector.prefix) + '%')
        .where('k', '>=', KyselyKv.encodeKey(selector.start));
    } else if (isPrefixUpto(selector)) {
      q = q.where('k', 'like', KyselyKv.encodeKey(selector.prefix) + '%')
        .where('k', '<', KyselyKv.encodeKey(selector.end));
    } else {
      q = q.where('k', '>=', KyselyKv.encodeKey(selector.start))
        .where('k', '<', KyselyKv.encodeKey(selector.end));
    }

    if (reverse) {
      return q.orderBy('k desc');
    }

    return q.orderBy('k asc');
  }

  /**
   * Create an iterator adapting Kysely records into Deno KvEntries.
   * @param selector The Deno.KvListSelector to iterate over.
   * @param options Options for the listing. Currently ignored.
   */
  async *listInternal<T = unknown>(
    selector: Deno.KvListSelector,
    options?: Deno.KvListOptions,
    // deno-lint-ignore no-explicit-any
  ): AsyncGenerator<Deno.KvEntry<T>, any, unknown> {
    // TODO: respect limits!
    const _limit = options?.limit || Number.MAX_SAFE_INTEGER;
    const batchSize = options?.batchSize || options?.limit || 100;
    if (batchSize > 500) throw new Error('Exceeded maximum batch size in call to limit()');
    const stream = this.getQuery(selector, options?.reverse).stream();
    for await (const itm of stream) {
      yield {
        key: KyselyKv.decodeKey(itm.k),
        value: KyselyKv.decodeVal(itm.v),
        versionstamp: KyselyKv.encodeVersionstamp(itm.version),
      };
    }
  }

  list<T = unknown>(selector: Deno.KvListSelector, options?: Deno.KvListOptions | undefined): Deno.KvListIterator<T> {
    const iter = this.listInternal<T>(selector, options);
    return new KyselyKvListIterator<T>(iter);
  }

  enqueue(
    _value: unknown,
    _options?: { delay?: number; keysIfUndelivered?: Deno.KvKey[]; backoffSchedule?: number[] },
  ): Promise<Deno.KvCommitResult> {
    throw new Error('Method not implemented.');
  }

  // deno-lint-ignore no-explicit-any
  listenQueue(_handler: (value: any) => void | Promise<void>): Promise<void> {
    throw new Error('Method not implemented.');
  }

  atomic(): KyselyKvAtomicOperation {
    return new KyselyKvAtomicOperation(this);
  }

  watch<T extends readonly unknown[]>(
    _keys: readonly [...{ [K in keyof T]: Deno.KvKey }],
    _options?: { raw?: boolean },
  ): ReadableStream<{ [K in keyof T]: Deno.KvEntryMaybe<T[K]> }> {
    // TODO: IMPLEMENT THIS!
    throw new Error('Not implemented yet.');
  }

  async close(): Promise<void> {
    await this.kysely.destroy();
  }

  commitVersionstamp(): symbol {
    throw new Error('Method not implemented.');
  }

  [Symbol.dispose](): void {
    this.kysely.destroy();
  }

  async migrate(): Promise<void> {
    await this.kysely.schema
      .createTable('last_version')
      .ifNotExists()
      .addColumn('i', 'integer', (c) => c.notNull().unique())
      .addColumn('v', 'numeric', (c) => c.notNull().unique())
      .execute();

    await this.kysely
      .insertInto('last_version')
      .values({ i: 0, v: BigInt(0) })
      .onConflict((c) => c.doNothing())
      .execute();

    await this.kysely.schema
      .createTable('kv')
      .ifNotExists()
      .addColumn('k', 'text', (c) => c.primaryKey())
      .addColumn('v', 'bytea', (c) => c.notNull())
      .addColumn('version', 'numeric', (c) => c.notNull())
      .execute();

    await this.kysely.schema
      .createIndex('key_asc_index')
      .ifNotExists()
      .on('kv')
      .column('k asc')
      .execute();

    await this.kysely.schema
      .createIndex('key_desc_index')
      .ifNotExists()
      .on('kv')
      .column('k asc')
      .execute();
  }

  /**
   * Increment the timestamp stored in the database by atleast 1 and at most 10,
   * and return it as a bigint. The returned value must be encoded with encodeVersionstamp.
   * @returns the version stamp.
   */
  async getNextVersionstamp(): Promise<bigint> {
    const inc = 1 + Math.round(Math.random() * 10);
    const result = await this.kysely
      .updateTable('last_version')
      .set((e) => ({ v: e('v', '+', BigInt(inc)) }))
      .where('i', '=', 0)
      .returning('v')
      .executeTakeFirstOrThrow();

    return result!.v;
  }

  /**
   * Run a transaction on the database and ignore results. This function
   * is only meant to be used by KyselyKvAtomicOperation.
   * @param f The transaction to be run.
   */
  async runTransaction(f: (fn: Transaction<KyselyKvTables>) => Promise<void>) {
    await this.kysely.transaction().execute(f);
  }
}
