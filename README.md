# KyselyKv

A module that adapts any Kysely storage into a Deno.Kv compatible key-value store.
The API is exactly identical to Deno.Kv and this can be used anywhere that
Deno.Kv can be used.

## Roadmap

Currently, `get()`, `set()`, `getMany()`, and `list()` methods are implemented.
`watch()` and `enqueue()` support is planned but not a priority.

## Usage

### Setup

```ts
import { DenoSqlite3Dialect } from 'jsr:@soapbox/kysely-deno-sqlite@^2.1.0';
import { Kysely } from 'npm:kysely@^0.27.3';
import { KyselyKv } from './KyselyKv.ts';
import { Database as Sqlite } from '@db/sqlite';
import { assertEquals } from '@std/assert';

const kv = new KyselyKv(
  new Kysely({
    dialect: new DenoSqlite3Dialect({ database: new Sqlite(':memory:') }),
  }),
);
```

### Setting values

```ts
await kv.set(['users', 'alice'], {
  name: 'Alice',
  follower_count: 12,
  bio: "I'm Alice! I love cats.",
});

await kv.set(['users', 'bob'], {
  name: 'Bob',
  follower_count: 14,
  bio: "I'm Bob! I love dogs.",
});
```

### Getting single values

```ts
assertEquals((await kv.get(['users', 'alice'])).name, 'Alice');
```

### Getting many values at once

```ts
const [alice, bob] = await kv.getMany([['users', 'alice'], ['users', 'bob']]);
console.log(
  alice.value.follower_count > bob.value.follower_count ? 'Alice' : 'Bob',
  'has the most followers out of alice and bob.',
);
// Output:
// Bob has the most followers out of alice and bob.
```

### Listing values (prefix search)

```ts
let idx = 0;
for await (const user of kv.list({ prefix: ['users'] })) {
  console.log(`User ${++idx}:`, user.name);
  console.log(`\t${user.follower_count}`, 'followers,');
  console.log(`\t"${user.bio}"`);
}
```

Range search, prefix search upto a certain key, prefix search starting at a
certain key are also supported. See [Deno.Kv documentation](https://deno.land/api@latest?s=Deno.Kv&unstable=&p=prototype.list)
for examples.

## License

[The MIT License](./LICENSE).
